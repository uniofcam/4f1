clear;
alpha = 0.05;
beta = 0;

% for alpha = 0.045:0.005:0.055
% for beta = -1:1:1

    G1=tf([1],[1 -0.1 alpha]);
    G2=tf([beta 0.1],[1 3 25]);
    G=G1+G2
    
    wc_lead=0.75; ph_lead=tand((50+90)/2);
    Kb=ph_lead*tf([1 wc_lead/ph_lead],[1 wc_lead*ph_lead])
    wc_lag=0.12; ph_lag=tand((28+90)/2);
    Kc=tf([1 wc_lag*ph_lag],[1 wc_lag/ph_lag])
    Ka =1/bode(feedback(G*Kc,Kb),0)
    
    OL=G*Kc*Kb
    CL=feedback(G*Kc,Kb)
    CLref=CL*Ka
    T=OL/(1+OL);
    D=tf([conv([beta 0.1],[1 -0.1 alpha])],[1 3 25]);

    hold on
    figure(1); margin(OL)
    figure(2); step(CLref)
    figure(3); bode(T,1/D)
    figure(5); nyquist(OL)
    figure(4); pzmap(OL)

% end
% controlSystemDesigner(G)