<!-- git checkout --orphan temp; git add -A; git commit -am "final"; git branch -D main; git branch -m main; git push -f origin main; git -c gc.reflogExpire=0 -c gc.reflogExpireUnreachable=0 -c gc.rerereresolved=0 -c gc.rerereunresolved=0 -c gc.pruneExpire=now gc "$@" -->

# 4F1
### Scripts for control system coursework

## Instructions

`run.ipynb` for tuning and analysis in Python  
`run.m` to generate verification in MATLAB  

![Screenshot](plots/comp-bode.png)
